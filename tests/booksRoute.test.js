const supertest = require('supertest');
const expect = require('chai').expect;

const { createBooksTable } = require('./seedDatabase.test.js');

// before('cleaning', async () => {
//   await createBooksTable();
//   console.log('done');
// });

describe('shoud GET /books', () => {
  it ('should response with 200 and body should contain json', async () => {
    let res;
    try {
      res = await supertest('http://localhost:3005')
      .get('/books');
    } catch (err) {
      console.log(err);
    }

    expect(res.headers['content-type']).to.equal('application/json; charset=utf-8')
    expect(res.statusCode).to.equal(200);
    expect(res.body[0]).to.include({book_id: 1, isbn: '123456789'});
  });
});

describe('shoud POST /books', () => {
  it ('should add book and response with 200', async () => {
    const form = {
      isbn: '12345555',
      title: 'test title',
      averageRating: 2.00,
      imageUrl: 'image url',
      goodreadsId: '12345',
      goodreadsUrl: 'goodreads url',
      description: 'description text',
      publicationDay: 11,
      publicationMonth: 12,
      publicationYear: 2000,
      author1: 1,
    };
    let res;
    try {
      res = await supertest('http://localhost:3005')
      .post('/books')
      .send(form)
      .set('Accept', 'application/json');
    } catch (err) {
      console.log(err);
    }

    expect(res.statusCode).to.equal(200);
    expect(res.body).to.include({ message: 'Successfully Added' });
  });
});

describe('shoud PUT /books/:bookId', () => {
  it ('should response with 200 and body should contain json', async () => {
    const form = {
      imageUrl: "update url",
      description: 'updated description text',
    };
    let res;
    try {
      res = await supertest('http://localhost:3005')
      .put(`/books/3`)
      .set('Accept', 'application/json')
      .send(form);
    } catch (err) {
      console.log(err);
    }

    expect(res.statusCode).to.equal(200);
    expect(res.body).to.include({ message: 'Successfully updated the changes' });
  });
});

describe('shoud DELETE /books/:bookId', () => {
  it ('should response with 200 and body should contain json', async () => {

    let res;
    try {
      res = await supertest('http://localhost:3005')
      .delete(`/books/3`);
    } catch (err) {
      console.log(err);
    }

    expect(res.statusCode).to.equal(200);
    expect(res.body).to.include({ message: 'Successfully deleted book with id 3' });
  });
});
