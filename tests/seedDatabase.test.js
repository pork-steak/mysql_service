const mysql = require('mysql2/promise');

const options = {
  host: 'localhost',
  user: 'root',
  password: '',
};

function connectToMysql() {
  return mysql.createConnection(options);
}

async function createDatabase(connection) {
  await connection.query('CREATE DATABASE IF NOT EXISTS dummy_rock');
  await connection.query('USE dummy_rock');
  console.log('Successfully connected to database');
  return connection;
}

async function clearBooks(connection) {
  try {
    await connection.query('TRUNCATE books');
  } catch (err) {
    console.error(err);
  }
}

async function clearAuthors(connection) {
  try {
    await connection.query('TRUNCATE authors');
  } catch (err) {
    console.error(err);
  }
}

async function createBooksTable(connection) {
  console.log('Creating Books Table');
  try {
    await connection.query(`CREATE TABLE IF NOT EXISTS books (
      book_id SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
      isbn CHAR(10) NOT NULL,
      title VARCHAR(200) NOT NULL,
      average_rating FLOAT(3,2) UNSIGNED,
      image_url VARCHAR(150),
      goodreads_id INT UNSIGNED NOT NULL,
      goodreads_url VARCHAR(150) NOT NULL,
      description TEXT,
      publication_day TINYINT UNSIGNED,
      publication_month TINYINT UNSIGNED,
      publication_year SMALLINT UNSIGNED,
      author_1 SMALLINT UNSIGNED,
      author_2 SMALLINT UNSIGNED,
      author_3 SMALLINT UNSIGNED,
      author_4 SMALLINT UNSIGNED,
      author_5 SMALLINT UNSIGNED,
      author_6 SMALLINT UNSIGNED,
      UNIQUE (isbn, goodreads_id) ) CHARACTER SET utf8 COLLATE utf8_general_ci
      `);
  } catch (err) {
    console.error('Creating books table failed');
    throw err;
  }
}

async function insertBook(connection) {
  const values = [
    '123456789',
    'test title',
    2.00,
    "image url",
    467,
    "goodreads url",
    "description text",
    11,
    12,
    2000,
    1,
  ]
  try {
    await connection.execute(`INSERT INTO books (isbn, title, average_rating, image_url,
      goodreads_id, goodreads_url, description, publication_day, publication_month,
      publication_year, author_1) VALUES (?,?,?,?,?,?,?,?,?,?,?)`, values);
  } catch (err) {
    console.error(err);
  }
}

async function insertAuthor(connection) {
  const values = [
    'supertest',
    'test url',
    5,
    'goodreads url',
    "hello there"
  ];
  try {
    await connection.execute(`INSERT INTO authors (name, image_url, goodreads_id,
      goodreads_url, description) VALUES (?,?,?,?,?)`, values);
  } catch (err) {
    console.error(err);
  }
}

async function createAuthorsTable(connection) {
  console.log('Creating Authors Table');

  try {
    await connection.query(`CREATE TABLE IF NOT EXISTS authors (
      author_id SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
      name VARCHAR(200) NOT NULL,
      image_url VARCHAR(150),
      goodreads_id INT UNSIGNED NOT NULL,
      goodreads_url VARCHAR(150) NOT NULL,
      description TEXT,
      UNIQUE (goodreads_id) )
      CHARACTER SET utf8 COLLATE utf8_general_ci`);
  } catch (err) {
    console.error('Creating authors table failed');
    throw err;
  }
}

function closeConnection(connection) {
  return connection.end();
}

module.exports.createAuthorTable = async function() {
  let connection;
  try {
    console.log('Creating connection');
    connection = await connectToMysql();
  } catch (err) {
    console.log('Cannot Connect to MySQL database, Check for bad mysql login input');
    console.error(err);
    process.exit();
  }

  try {
    await createDatabase(connection);
    await createAuthorsTable(connection);
    await clearAuthors(connection);
    console.log('authors Table created successfully');
    await insertAuthor(connection);
    console.log('author inserted');
  } catch (err) {
    // errorsLogger.error(err);
    console.error(err.stack);
  } finally {
    closeConnection(connection);
  }
}

module.exports.createBooksTable = async function() {
  let connection;
  try {
    connection = await connectToMysql();
  } catch (err) {
    console.log('Cannot Connect to MySQL database, Check for bad mysql login input');
    console.error(err);
    process.exit();
  }

  try {
    await createDatabase(connection);
    await createBooksTable(connection);
    await clearBooks(connection);
    console.log('books table created successfully');
    await insertBook(connection);
    console.log('inserted a book successfully');
  } catch (err) {
    // errorsLogger.error(err);
    console.error(err.stack);
  } finally {
    closeConnection(connection);
  }
}
