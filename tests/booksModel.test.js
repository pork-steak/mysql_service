const supertest = require('supertest');
const expect = require('chai').expect;

const { createBooksTable } = require('./seedDatabase.test.js');
const {
  getBooks,
  getBookById,
  addBook,
  updateBook,
  deleteBook,
} = require('../src/booksModel.js')

before('cleaning', async () => {
  await createBooksTable();
});

describe('should return all rows', () => {
  it ('should return all rows from books table', async () => {
    let result;
    try {
      [result] = await(getBooks());
    } catch (err) {
      console.error(err);
    }
    expect(result[0].book_id).to.equal(1);
    expect(result[0].isbn).to.equal('123456789');
  });
});

describe('should get Book by id', () => {
  it ('should return one row of book', async() => {
    let result;
    try {
      [result] = await(getBookById(1));
    } catch (err) {
      console.error(err);
    }
    expect(result[0].book_id).to.equal(1);
    expect(result[0].isbn).to.equal('123456789');
  });
});

describe('should add Book', () => {
  it ('should add book and inserted ID should equal 2', async() => {
    let result;
    const bookDetails = [
      '987654321',
      'new title',
      2.00,
      "image url",
      121,
      "goodreads url",
      "description text",
      11,
      12,
      2000,
      1,
    ]
    try {
      [result] = await(addBook(bookDetails));
    } catch (err) {
      console.error(err);
    }
    expect(result.affectedRows).to.equal(1);
    expect(result.insertId).to.equal(2);
  });
});

describe('should update Book by id', () => {
  it ('should update book details', async() => {
    const fields = ['isbn=?', 'image_url=?'];
    const values = ['999999999', 'new url'];
    let result;
    try {
      [result] = await(updateBook(fields, values, 2));
    } catch (err) {
      console.error(err);
    }
    expect(result.affectedRows).to.equal(1);
  });
});

describe('should delete Boook by id', () => {
  it ('should delete 1 book row', async() => {
    let result;
    try {
      [result] = await(deleteBook(2));
    } catch (err) {
      console.error(err);
    }
    expect(result.affectedRows).to.equal(1);
  });
});
