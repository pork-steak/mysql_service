const supertest = require('supertest');
const expect = require('chai').expect;

const { createAuthorTable } = require('./seedDatabase.test.js');

// before('cleaning', async () => {
//   await createAuthorTable();
// });

describe('shoud GET /authors', () => {
  it ('should response with 200 and body should contain json', async () => {
    let res;
    try {
      res = await supertest('http://localhost:3005')
        .get('/authors');
    } catch (err) {
      console.log(err);
    }

    expect(res.headers['content-type']).to.equal('application/json; charset=utf-8');
    expect(res.statusCode).to.equal(200);
    expect(res.body[0]).to.include({author_id: 1, name: 'supertest'});
  });
});

describe('shoud POST /authors', () => {
  it ('should add book and response with 200', async () => {
    const form = {
      name: 'runtest',
      imageUrl: 'test',
      goodreadsId: 2,
      goodreadsUrl: 'url',
      description: 'hello there'
    };
    let res;
    try {
      res = await supertest('http://localhost:3005')
        .post('/authors')
        .send(form)
        .set('Accept', 'application/json');
    } catch (err) {
      console.log(err);
    }

    expect(res.statusCode).to.equal(200);
    expect(res.body).to.include({ message: 'Successfully added' });
  });
});

describe('shoud PUT /authors/:authorId', () => {
  it ('should response with 200 and body should contain json', async () => {
    const form = {
      imageUrl: 'new url',
      description: 'updated hello there'
    };
    let res;
    try {
      res = await supertest('http://localhost:3005')
        .put('/authors/3')
        .set('Accept', 'application/json')
        .send(form);
    } catch (err) {
      console.log(err);
    }

    expect(res.statusCode).to.equal(200);
    expect(res.body).to.include({ message: 'Successfully updated the changes' });
  });
});

describe('shoud DELETE /authors/:authorId', () => {
  it ('should response with 200 and body should contain json', async () => {
    let res;
    try {
      res = await supertest('http://localhost:3005')
        .delete('/authors/3');
    } catch (err) {
      console.log(err);
    }

    expect(res.statusCode).to.equal(200);
    expect(res.body).to.include({ message: 'Successfully deleted author with id 3, and books written by the same author' });
  });
});
