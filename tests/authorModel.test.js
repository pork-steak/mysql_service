const supertest = require('supertest');
const expect = require('chai').expect;

const { createAuthorTable } = require('./seedDatabase.test.js');
const {
  getAuthors,
  getAuthorById,
  addAuthor,
  updateAuthor,
  deleteAuthor,
} = require('../src/authorsModel.js')

before('cleaning', async () => {
  await createAuthorTable();
});

describe('should return all rows', () => {
  it ('should return all rows from authors table', async () => {
    let result;
    try {
      [result] = await(getAuthors());
    } catch (err) {
      console.error(err);
    }
    expect(result[0].author_id).to.equal(1);
    expect(result[0].name).to.equal('supertest');
  });
});

describe('should get Author by id', () => {
  it ('should return one row of author', async() => {
    let result;
    try {
      [result] = await(getAuthorById(1));
    } catch (err) {
      console.error(err);
    }
    expect(result[0].author_id).to.equal(1);
    expect(result[0].name).to.equal('supertest');
  });
});

describe('should add Author', () => {
  it ('should add author', async() => {
    let result;
    const authorDetail = [
      'New Name',
      'new url',
      100,
      'new gr url',
      'new description',
    ];
    try {
      [result] = await(addAuthor(authorDetail));
    } catch (err) {
      console.error(err);
    }
    expect(result.affectedRows).to.equal(1);
    expect(result.insertId).to.equal(2);
  });
});

describe('should update Author by id', () => {
  it ('should update author row', async() => {
    const fields = ['name=?', 'image_url=?'];
    const values = ['New Name', 'new url'];
    let result;
    try {
      [result] = await(updateAuthor(fields, values, 2));
    } catch (err) {
      console.error(err);
    }
    expect(result.affectedRows).to.equal(1);
  });
});

describe('should delete Author by id', () => {
  it ('should delete author row', async() => {
    let result;
    try {
      [result] = await(deleteAuthor(2));
    } catch (err) {
      console.error(err);
    }
    expect(result.affectedRows).to.equal(1);
  });
});
