module.exports = {
  apps: [{
    name: 'mysqlapi',
    script: './mysql_service/server.js',
  }],
  deploy: {
  // "production" is the environment name
    production: {
      // SSH key path, default to $HOME/.ssh
      key: '~/.ssh/keypair.pem',
      // SSH user
      user: 'ubuntu',
      // SSH host
      host: ['13.233.199.114'],
      // SSH options with no command-line flag, see 'man ssh'
      // can be either a single string or an array of strings
      // ssh_options: "StrictHostKeyChecking=no",
      // GIT remote/branch
      ref: 'origin/master',
      // GIT remote
      repo: 'git@gitlab.com:pork-steak/mysql_service.git',
      // path in the server
      path: '/home/ubuntu/mysql_service',
      // Pre-setup command or path to a script on your local machine
      'pre-setup': 'echo "making dir mysql_service"; [ -d mysql_service ] || mkdir mysql_service',
      // Post-setup commands or path to a script on the host machine
      // eg: placing configurations in the shared dir etc
      'post-setup': 'npm install',
      // pre-deploy action
      'pre-deploy-local': "echo 'Deploying now...'",
      // post-deploy action
      'post-deploy': 'npm start',
    },
  },
};

// log_type”: “json "log_date_format": "DD-MM-YYYY"
