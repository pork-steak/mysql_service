const express = require('express');
const bodyParser = require('body-parser');

const routes = require('./src');

const app = express();
const port = 3005;

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE');
  next();
});

app.use((req, res, next) => {
  console.log(req.url);
  next();
});

app.use('/', routes);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
