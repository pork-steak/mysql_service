const express = require('express');

const {
  getAuthors,
  getAuthorById,
  addAuthor,
  updateAuthor,
  deleteAuthor,
} = require('./authorsModel.js');

const router = express.Router();

module.exports = router;

// router for authors / authors [{}, {}, ...]
router.get('/', async (req, res, next) => {
  try {
    const [authors] = await getAuthors();
    if (authors.length > 0) {
      res.json(authors);
    }
  } catch (err) {
    next(err);
  }
});

// get Author by ID / author [{},[{}]]
router.get('/:authorId', async (req, res, next) => {
  try {
    const [author] = await getAuthorById(req.params.authorId);
    if (author.length === 0) {
      res.status(404);
      res.json({ error: 'no resource found using the specified author id' });
      next(new Error('Author not found'));
    } else {
      res.json(author);
    }
  } catch (err) {
    next(err);
  }
});

// add author
router.post('/', async (req, res, next) => {
  // console.log('adding author');
  const authorDetails = Object.values(req.body);
  try {
    const [field] = await addAuthor(authorDetails);
    if (field.affectedRows === 1) {
      res.json({ message: 'Successfully added' });
    }
  } catch (err) {
    res.status(500);
    res.json({ error: err.message });
    next(err);
  }
});

// update author by ID
router.put('/:authorId', async (req, res, next) => {
  const fields = [];
  const values = [];
  if (req.body.name) {
    fields.push('name=?');
    values.push(req.body.name);
  }

  if (req.body.imageUrl) {
    fields.push('image_url=?');
    values.push(req.body.imageUrl);
  }

  if (req.body.goodreadsId) {
    fields.push('goodreads_id=?');
    values.push(+req.body.goodreadsId);
  }

  if (req.body.goodreadsUrl) {
    fields.push('goodreads_url=?');
    values.push(req.body.goodreadsUrl);
  }

  if (req.body.description) {
    fields.push('description=?');
    values.push(req.body.description);
  }

  try {
    const [field] = await updateAuthor(fields, values, +req.params.authorId);
    if (field.affectedRows === 1) {
      res.json({ message: 'Successfully updated the changes' });
    }
  } catch (err) {
    res.json({ error: err.message });
    next(err);
  }
});

// delete author by ID
router.delete('/:authorId', async (req, res, next) => {
  const id = req.params.authorId;
  try {
    const [field] = await deleteAuthor(id);
    if (field.affectedRows === 0) {
      res.json({ error: "The author ID you've specified does not exists" });
    } else if (field.affectedRows === 1) {
      res.json({ message: `Successfully deleted author with id ${id}, and books written by the same author` });
    }
  } catch (err) {
    next(err);
  }
});
